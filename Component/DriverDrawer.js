import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, AsyncStorage, ImageBackground } from "react-native";
import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {connect} from 'react-redux';
import Action from '../Component/store/action/Actions'

class DriverDrawerMenu extends Component{
  constructor(){
    super();
    this.state= {
        name: '',
        email: '',
        status: '',
      categoryNav: [
        {title: 'POINT LOCATION', link: 'DriverLocation', icon: 'location-pin'},
        {title: 'SETTINGS', link: 'SettingScreen', icon: 'settings'},
        {title: 'LOGOUT',icon: 'log-out', func: () => {
            AsyncStorage.removeItem('user')
            this.props.logout();
            this.logout();
        }
        }
      ]
    }
  }

  componentDidMount(){
      this.getData();
  }

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('user').then(val => {
        if (val !== null) {
          let parsed = JSON.parse(val);
          this.setState({
              name: parsed.firstName+" "+ parsed.lastName,
              email: parsed.email,
              status: parsed.status
          })
        }
        else{
            const {parsed} = this.props
              this.setState({
                name: parsed.firstName+" "+ parsed.lastName,
                email: parsed.email,
                status: parsed.status
            })
        }
      })
    }   
    catch (e) {}
  }

  logout = () => {
      const { logout } = this.props.screenProps;
      logout('logout')
  }

  onSelectItem(item) {
    if(item.link) {
        this.props.navigation.navigate(item.link) 
    }
    else item.func();
}

  renderMenuItem({ item }) {
    return (
        <TouchableOpacity style={{ flex: 1,paddingHorizontal: 15,}} onPress={() => this.onSelectItem(item)}>
            <View style={{ paddingVertical:  12,flexDirection: 'row',alignItems: 'center'}}>
                {item.icon === 'location-pin' || item.icon === 'log-out' ? <Icon  name={item.icon} size={25} color="green"/> : <MaterialIcons  name={item.icon} size={25} color="green"/>}
                <Text style={{ fontSize: 15.3,color: '#2a2a2a', flex: 1, marginHorizontal: 15, fontFamily: 'Raleway-Medium'}}>{item.title}</Text>
            </View>
        </TouchableOpacity>
    );
}

  render(){
      const {name, email, status} = this.state;
    return(
      <View style = {{backgroundColor: '#f2f2f2', flex:1}}>
          <ImageBackground style={{width: '100%', height: 175, justifyContent: 'center'}} source={require('../Images/drawer-banner.png')}>
              <View style={{flexDirection: 'row', paddingLeft: 20,paddingRight: 10}}>
                  <Image style={{ width: 70, height: 70, marginRight: 15}} source={require('../Images/avatar.png')} />
                  <View style={{flex: 1}}>
                      <Text style={{color: 'white', fontSize: 18, marginTop: 12, marginBottom: 3, fontFamily: 'Raleway-Bold'}}>{name}</Text>
                      <Text style={{color: 'white', fontSize: 14, width: '100%', fontFamily: 'Raleway-Regular'}}>{email}</Text>
                      <Text style={{color: 'white', fontSize: 14, width: '100%', fontFamily: 'Raleway-Regular'}}>{status}</Text>                      
                  </View>
              </View>
          </ImageBackground>
      <ScrollView>
          <View style={{flex: 1, paddingHorizontal: 15}}>
                <FlatList
                  data={this.state.categoryNav}
                  renderItem={this.renderMenuItem.bind(this)}
                  keyExtractor={(item)=> item.title} />
          </View>
      </ScrollView>
  </View>
    )
  } 
}

function mapStateToProps(state){
    return{
        parsed: state.AUTHENTICATION
    }
}

function mapDispatchToProps(dispatch){
    return{
        logout: function(){
            return dispatch(Action.ResetState())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverDrawerMenu)