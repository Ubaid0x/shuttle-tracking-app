import ActionTypes from '../action/actiontype';

const initial_State = {
   allUser: [],
   isLoading: true,
}

function AUTHENTICATION(state = initial_State , action)
{
    switch(action.type){

        case ActionTypes.NODESIGNUP : {
            return Object.assign({}, state, action.val)
        }
        case ActionTypes.NODESIGNUPFAILURE : {
            return {
                alert: action.alert
            }
        }
        case ActionTypes.NODELOGIN: {
            return Object.assign({}, state, action.val)
        }
        case ActionTypes.ALLUSERS: {
            return{
                ...state,
                allUser: action.val
            }
        }
        case ActionTypes.PASSWORDNOTMATCHED : {
            return {
            }
        }
        case ActionTypes.RESET : {
            return initial_State
        }
        default : {
            return state   
        } 
    }
}
export default AUTHENTICATION