import Action from '../action/Actions';
import {AsyncStorage, Alert} from 'react-native';
// import AppNavigation from '../../Routes/routes';

save = async (user) => {
    try {
      await AsyncStorage.setItem('user', JSON.stringify(user));
    } catch (e) {
      Alert.alert(e)
    }
  }

export default class Middlewarefunc {

    static nodeSignUp(state, props){
        return(dispatch) =>{
            if(state.password === state.confirmPsw){
                fetch('https://rocky-stream-13314.herokuapp.com/signup',{
                    method : 'POST',
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                    body : JSON.stringify({
                        firstName : state.firstName,
                        lastName : state.lastName,
                        gender: state.gender,
                        email: state.email.toLocaleLowerCase(),
                        password: state.password,
                        longitude: state.longitude,
                        latitude: state.latitude,
                        status: state.status,
                    })
                }).then((res)=>{
                    res.json().then(function(data){
                        if(data.message === 'Successfully Registerd'){
                            dispatch(Action.NODESIGNUP(data.obj, data.message))
                            this.save(data.obj)
                        }
                        else{
                            dispatch(Action.NODESIGNUPFAILURE(data.message))
                            props.navigation.navigate('Login')
                        }
                    })
                })

            }
            else
            {
                //password Not Matched!!
                dispatch(Action.PASSWORDNOTMATCHED())
            }
        }
    }
    
    static nodeLogin(state,props){
        return(dispatch) =>{
            fetch('https://rocky-stream-13314.herokuapp.com/login',{
                method : 'POST',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body : JSON.stringify({
                    email: state.email.toLocaleLowerCase(),
                    password: state.password,
                })
            }).then((res)=>{
                res.json().then(function(data){
                   if(data.message === 'Successfully Login'){
                       dispatch(Action.NODELOGIN( data.record, data.message ))
                       this.save(data.record)
                       props.navigation.navigate('Home')
                   }
                   else{
                    dispatch(Action.NODESIGNUPFAILURE(data.message))
                   }
                })
            })
        }
    }
    static addDriver(state,props){
        return(dispatch) =>{
                fetch('https://rocky-stream-13314.herokuapp.com/signup',{
                    method : 'POST',    
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                    body : JSON.stringify({
                        firstName : state.firstName,
                        lastName : state.lastName,
                        point_location: state.point_location,
                        email: state.email.toLocaleLowerCase(),
                        password: state.password,
                        longitude: state.longitude,
                        latitude: state.latitude,
                        status: state.status,
                    })
                }).then((res)=>{
                    res.json().then(function(data){ 
                        if(data.message === 'Successfully Registerd'){
                            dispatch(Action.NODESIGNUP( data.obj, data.message ))
                        }
                        else{
                            dispatch(Action.NODESIGNUPFAILURE(data.message))
                            props.navigation.navigate('AddDriver')
                        }
                    })
                })
            }
        }
        static getUsers(){
            return(dispatch) =>{
                fetch(`https://rocky-stream-13314.herokuapp.com/getData`,{
                    method: 'GET',
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                }).then((res) => {
                    res.json().then(function(data){
                        dispatch(Action.ALLUSERS(data))
                    })
                })
            }
        }
    }
