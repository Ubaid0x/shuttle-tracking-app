import ActionTypes from '../action/actiontype';
import { Alert, ToastAndroid } from 'react-native'


const Action  = {
    NODESIGNUP: function(user, alert){ 
        ToastAndroid.showWithGravityAndOffset(`${alert}`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        return{
            type: ActionTypes.NODESIGNUP,
            val: user
        }
    },
    PASSWORDNOTMATCHED: function(){
        Alert.alert('Error', 'Password Not Matched!!')
        return{
            type: ActionTypes.PASSWORDNOTMATCHED
        }
    },
    NODESIGNUPFAILURE: function(alert){
        Alert.alert('Warning', alert);        
        return{
            type: ActionTypes.NODESIGNUPFAILURE,
            alert
        }
    },
    NODELOGIN: function(user, alert){
        ToastAndroid.showWithGravityAndOffset(`${alert}`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        return{
            type: ActionTypes.NODELOGIN,
            val: user
        }
    },
    ALLUSERS: function(data){
        return{
            type: ActionTypes.ALLUSERS,   
            val: data
        }
    },
    ResetState : function(){
        return{
            type : ActionTypes.RESET
        }
    }
}

export default Action;