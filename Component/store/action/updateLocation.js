import ActionTypes from '../action/actiontype';

const UpdateLocationAction  = {
    UPDATELOCATION: function(data){ 
        return{
            type: ActionTypes.lOCATION,
            val: data
        }
    },
}
export default UpdateLocationAction;
