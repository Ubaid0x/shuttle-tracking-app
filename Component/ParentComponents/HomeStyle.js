import { StyleSheet, Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');

export default TraceStyle = StyleSheet.create({
    map  : {
        ...StyleSheet.absoluteFillObject
    },
    mapButton: {
        // 
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: 'green'
    },
    btnView: {
        paddingTop:13,
        justifyContent: 'center', 
        alignItems: 'center',
    },
    btnAlignmentEnd: {
        alignItems: 'flex-end',
        marginTop: height/1.33,
        marginRight: width/100*6.3
    },
    modal3: {
        height: 200,
        width: 300
    },
    modal: {
        // justifyContent: 'center',
        // alignItems: 'center'
        height: 120
    },
    modelHeader: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        fontFamily: 'Raleway-Regular'
    },
    container2 : {  
        flexDirection: 'column',
        // alignItems: 'flex-start',
        marginTop: 15,
        marginLeft: 15
    },
    underline: {
        flexDirection : 'row',
        alignItems: 'center'
    },
    position1 : {
        paddingLeft : 15 ,
        fontFamily: 'Raleway-Medium',
        fontSize : 15
    },
    button1 : {
        width: 200,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 38,
        borderBottomWidth: 0.8,
        borderBottomColor: 'gray'
    },
    textfield: {
        width: width*5/10, 
        fontFamily: 'Raleway-Regular', 
        color:'#666',
    }
});