import React from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage, Alert} from 'react-native';
import MapView from 'react-native-maps';
import styles from '../TraceLocation/TraceLocationStyle'
import io from 'socket.io-client/dist/socket.io';
import { connect } from 'react-redux';
import UpdateLocationAction from '../../Component/store/action/updateLocation';

class DriverLocation extends React.Component{
    constructor(){
        super();
        this.state = {
            mapRegion : null,
            lastLat : null,
            lastLong : null,
            region : '',
        };
        this.socket = io.connect('https://rocky-stream-13314.herokuapp.com', {jsonp: false});
        let watchID = null;
    }

    componentDidMount(){
        this.location()
        this.check = setInterval(() => this.location(), 8000)
    }
    componentWillUnmount(){
        navigator.geolocation.clearWatch(this.watchID)
        clearInterval(this.check)
    }
    location(){
        const {_id} = this.props.screenProps;
        this.watchId = navigator.geolocation.getCurrentPosition((position) => {
            this.socket.emit('location', {
                position: position,
                id: _id  
            })    
            this.props.updateLocation(position);
                fetch(`https://rocky-stream-13314.herokuapp.com/updateLocation/${_id}`,{
                    method: 'POST',
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                    body: JSON.stringify({ 
                        longitude: position.coords.longitude,
                        latitude: position.coords.latitude,
                    })
                }).then((res,response)=>{
                    if(res){}   
                })
            let region = {
                latitude : position.coords.latitude,
                longitude : position.coords.longitude,
                latitudeDelta : 0.00922*1.5,
                longitudeDelta : 0.00421*1.5
            }
            this.setState({
                mapRegion : region,
                lastLat : region.latitude,
                lastLong : region.longitude
            });
          });
      }

    render(){
        return(
                <MapView style={styles.map}
                        region={this.state.mapRegion}
                        showsUserLocation={true}
                        followsUserLocation={true}>
                    <MapView.Marker draggable
                        coordinate={{
                        latitude: (this.state.lastLat) || -36.82339,
                        longitude: (this.state.lastLong) || -73.03569,
                        }}>
                    </MapView.Marker>
                </MapView>
        )
    }
}
function mapStateToProps(state){
    return{

    }
}
function mapDispatchToProps(disptach){
    return{
        updateLocation: function(data){
            return disptach(UpdateLocationAction.UPDATELOCATION(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DriverLocation)
