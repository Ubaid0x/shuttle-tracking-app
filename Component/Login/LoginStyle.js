import { StyleSheet } from 'react-native';

export default LoginStyle = StyleSheet.create({
    container: {
        // flex: 1,
        // flexDirection: 'column',
        justifyContent : 'center',  
        alignItems : 'center',
      },
      container1: {
          flex: 1,
          justifyContent : 'center',
          alignItems : 'center',
      },
      setLogin : {
          flex : 1,
          justifyContent : 'center',
          alignItems : 'center',
      },
      container2: {
        flex: 0,
        justifyContent : 'center',
        alignItems : 'center',
    },
      login2 : {
        height : 440,
        width : 300,
        justifyContent : 'center',
        alignItems : 'center',
        shadowOffset: {
            width: -6,
            height: -6,
          },
        shadowRadius : 6,
        shadowColor: '#212121',
        borderRadius : 40,
        elevation:6,
        backgroundColor : 'white' ,
    },
    marginemail:{
        marginTop:-13
    },
    marginpass:{
        marginTop:-13
    },
      login : {
          height : 400,
          width : 300,
          alignItems : 'center',
          shadowOffset: {
              width: -6,
              height: -6,
            },
          shadowRadius : 6,
          shadowColor: '#212121',
          borderRadius : 40,
          elevation: 6,
          backgroundColor : 'white' ,
          marginTop:7
      },
      loginbutton: {
          borderBottomWidth:1,
      },
      textField : {
         marginBottom : 10 ,
         paddingBottom : 15, 
         paddingLeft : 15,
         paddingRight : 15,
         borderBottomWidth : 0 ,
      },
      username : {
        marginTop : 40,
      },
      underline : {
          borderBottomWidth : 1,
          borderColor : 'black',
      },
      input : {
           marginTop : 10, 
      },
      btnLogin : {
          margin : 40,
      },
      bgstyle : {
          width : '100%',
          alignItems : 'stretch',
          height : '100%',
      },
    icons : {
            color : '#096b09',
            borderColor : 'white',
            fontSize : 35,
        },
    iconsgender:{
          color : '#096b09',
          borderColor : 'white',
          fontSize : 32,
    },
    iconslock:{
        color : '#096b09',
        borderColor : 'white',
        fontSize : 35,
        marginLeft:2
  },
  iconsenvelope : {
    color : '#096b09',
    borderColor : 'white',
    fontSize : 27,
},

    head : {
        fontSize : 23,
        fontFamily: 'Raleway-Light',
        justifyContent : 'flex-start',
        marginTop:10,
        borderColor:'#096b09'
        
  },
    loginhead:{
        fontSize : 23,
          fontFamily: 'Raleway-Light',
          justifyContent : 'flex-start',
          marginTop:5
    },
    gendertxt : {
        marginTop:5,
            fontSize : 19,
            fontFamily: 'Raleway-Light',
            justifyContent : 'flex-start',
    },
    txt : {
        fontSize : 17,
        fontFamily: 'Raleway-Light',
        justifyContent : 'flex-start',
    },
    button : {
        borderRadius : 8,
        margin : 40,  
        backgroundColor : '#0b6623',
    },
    btnText : {
        color : 'white',
        fontSize : 20,
        fontFamily: 'Raleway-Light',
    },
    signbtn : {
        borderColor : 'black',
        borderRadius : 15 ,
        backgroundColor : 'green',
        height : 45 ,
        width :140 ,
        justifyContent : 'center',
        alignItems : 'center',
    },
    signupText :{
        flex : 1,
        fontSize : 20,
        color : 'white',
        textAlign : 'center',
        paddingTop : 10 ,
        fontFamily: 'Raleway-Light',
    }
});
