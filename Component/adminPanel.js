import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import DriverList from './Tabs/DriverList';
import StudentList from './Tabs/StudentList';
import ParentList from './Tabs/ParentList';

import { createBottomTabNavigator } from 'react-navigation';

export default createBottomTabNavigator({
    DriverList: {
        screen: DriverList,
        navigationOptions: {
            tabBarLabel: 'DriverList',
            tabBarIcon: ({ tintColor }) =>(
                <Icon name="list" color={tintColor} size={24} />
            )
        }
    },
    StudentList: {
        screen: StudentList,
        navigationOptions: {
            tabBarLabel: 'StudentList',
            tabBarIcon: ({ tintColor }) =>(
                <Icon name="list" color={tintColor} size={24} />
            )
        }
    },
    ParentList: {
        screen: ParentList,
        navigationOptions: {
            tabBarLabel: 'ParentList',
            tabBarIcon: ({ tintColor }) =>(
                <Icon name="list" color={tintColor} size={24} />
            )
        }
    },
  },{
    initialRouteName: 'DriverList',
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
        activeTintColor: 'green',
        inactiveTintColor: 'grey',
        style: {
          backgroundColor: '#f2f2f2',
          borderTopWidth: 0.5,
          borderTopColor: 'grey'
        },
        indicatorStyle: {
          height: 0
        },
        showIcon: true
      }
  })
