import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Entypo';
import CustomRouteStyle from './appRouteStyles';

import DrawerMenu from '../Drawer';
import Login from '../Login/Login';
import Signup from '../SignUp/Signup';
import HomeScreen from '../../Component/Home/HomeScreen';
import TraceLocation from '../../Component/TraceLocation/TraceLocation';
import SettingScreen from '../SettingScreen';
import DriverLocation from '../../Component/DriverLocation/DriverLocation';


const StackMaterial = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: ({ navigation })=> {
            return{
                headerTitle: "HOME",
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle:  CustomRouteStyle.homeHeaderTitleStyle,
                headerLeft: <Icon name='menu' size={30} color="white" 
                            onPress={()=>navigation.toggleDrawer()}/>                
            }
        }
    },

    TraceLocation: {
        screen: TraceLocation,
        navigationOptions:({ navigation }) => {
            return{
                headerTitle: "TRACK POINT",
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.homeHeaderTitleStyle,
                headerLeft: <Icon name='menu' size={30} color="white" 
                            onPress={()=>navigation.openDrawer()}/>                
            }
        }
    },

    SettingScreen: {
        screen: SettingScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: "SETTINGS",
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <Icon name='menu' size={30} color="white" 
                            onPress={()=>navigation.openDrawer()}/>                
            }
        }
    },

    DriverLocation: {
        screen: DriverLocation,
        navigationOptions: ({navigation}) => {
            return{
                headerTitle: "DRIVER LOCATION",
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <Icon name='menu' size={30} color="white" 
                            onPress={()=>navigation.openDrawer()}/>  
            }
        }
    },
    Login: {
        screen: Login,
    },
    Signup: {
        screen: Signup,
    }

},{
    initialRouteName: 'Login'
});

export default AppNavigator = createDrawerNavigator({
    Home: { screen: StackMaterial }
},{
    contentComponent: DrawerMenu
})