import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Entypo';
import CustomRouteStyle from './appRouteStyles';

import StudentDrawerMenu from '../StudentDrawer';
import DriverMenu from '../DriverDrawer';
import ParentDrawerMenu from '../Drawers/ParentDrawer';
import AdminDrawer from '../AdminDrawer';
import Login from '../Login/Login';
import Signup from '../SignUp/Signup';
import ParentScreen from '../ParentComponents/Home';
import HomeScreen from '../../Component/Home/HomeScreen';
import TraceLocation from '../../Component/TraceLocation/TraceLocation';
import SettingScreen from '../SettingScreen';
import DriverLocation from '../../Component/DriverLocation/DriverLocation';
import AdminPanel from '../adminPanel';
import AddDriver from '../AddDriver';

export const Auth = createStackNavigator({
    Login: {
        screen: Login
    },
    Signup:{
        screen: Signup
    }
},{
    initialRouteName: 'Login',
    navigationOptions : () => ({
    header:null
})
})

export const StudentStack = createStackNavigator ({
    Home:{
        screen: HomeScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>HOME</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>           
            }
        }
    },
    TraceLocation:{
        screen: TraceLocation,
        navigationOptions: ({navigation})=>{
            const location = navigation.state.params.point_location;
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>{location}</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View> ,     
                headerRight: <View style={{width: 10}}/>          
            }
        }
    },
    SettingScreen: {
        screen: SettingScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>SETTINGS</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>                
            }
        }
    },
})

export const StudentStackDrawer = createDrawerNavigator ({
    Home: StudentStack
   },{
       contentComponent: StudentDrawerMenu
   })

export const ParentStack = createStackNavigator ({
    ChildLocation:{    
        screen: ParentScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>HOME</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>           
            }
        }
    },
    SettingScreen: {
        screen: SettingScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>SETTINGS</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>                
            }
        }
    }
})

export const ParentStackDrawer = createDrawerNavigator ({
    Home: ParentStack
   },{                                     
       contentComponent: ParentDrawerMenu       
   })

const DriverStack = createStackNavigator({
    DriverLocation:{
        screen: DriverLocation,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>DRIVER LOCATION</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>, 
                headerRight: <View style={{width: 10}}/>                 
            }
        }
    },
    SettingScreen:{
        screen: SettingScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>SETTINGS</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>                   
            }
        }
    }
})

export const DriverStackDrawer = createDrawerNavigator ({
 Home: DriverStack
},{
    contentComponent: DriverMenu
})

const AdminStack = createStackNavigator({
    AdminPanel:{
        screen: AdminPanel,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>ADMIN PANEL</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>, 
                headerRight: <View style={{width: 10}}/>                 
            }
        }
    },
    AddDriver:{
        screen: AddDriver,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>ADD DRIVER</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>                   
            }
        }
    },
    SettingScreen:{
        screen: SettingScreen,
        navigationOptions: ({navigation})=>{
            return{
                headerTitle: <View style={CustomRouteStyle.headerTitleView}><Text style={CustomRouteStyle.headerTitleText}>SETTINGS</Text></View>,
                headerStyle: CustomRouteStyle.headerBackground,
                headerTitleStyle: CustomRouteStyle.headerTitleStyle ,
                headerLeft: <View style={CustomRouteStyle.menuIcon}>
                                <Icon name='menu' size={30} color="white" 
                                onPress={()=>navigation.openDrawer()}/>
                            </View>,
                headerRight: <View style={{width: 10}}/>                   
            }
        }
    }
})
export const AdminStackDrawer = createDrawerNavigator ({
    Home: AdminStack
   },{
       contentComponent: AdminDrawer
   })
