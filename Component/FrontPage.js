import React from 'react';
import {Platform,StyleSheet,Text,View ,Image} from 'react-native';
import Header from '../Images/Group15.png';
import Logo from '../Images/Group10.png';
import Footer from '../Images/Group16.png';
import { Button } from 'native-base';
import {Link} from 'react-router-native'

class Frontpage extends React.Component {
  constructor(){
    super();
  }
  check = () => {
    this.props.history.push('/login')
  }
    render() {
      return (
        <View style={styles.container}>
          <View style = {styles.image}>
            <Image source={Header}/>
          </View>
          <View style={styles.logo}>
            <Image source={Logo}/>
          </View>
          <View>
           <Button onPress={this.check} block rounded style={styles.button}>
              <Text style = {styles.btnText}>Let's Track</Text>
          </Button>  
          </View>
          <View>
          <Image source={Footer} style={styles.footer}/>
          </View>
        </View>
      );
    }
  }
  export default Frontpage;
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
    },
    logo:{
      marginLeft : 10,   
    },
    image : {
      alignSelf : 'stretch',
      margin : 0 ,
    },
    footer: {
      width : null,
      backgroundColor: '#F5FCFF',
    },  
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    button : {
      margin : 40,  
      backgroundColor : '#0b6623',
    },
    btnText : {
      color : 'white',
      fontSize : 20,
      fontFamily: 'Raleway-Light',
    }
  });
  