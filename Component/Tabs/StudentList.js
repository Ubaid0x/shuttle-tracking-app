import React from 'react';
import {ToastAndroid, Platform, StyleSheet, Text, View, TouchableHighlight, ScrollView, ActivityIndicator, FlatList } from 'react-native';
import { Container, Input, Item } from 'native-base';
import styles from '../css';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconCross from 'react-native-vector-icons/Entypo';

import { connect } from 'react-redux';

class StudentList extends React.Component {
    constructor(){
        super();
        this.state={
            studentList: [],
            copyStudentList: []
        }
    }
    componentDidMount(){
        let users = this.props.allUsers;
        studentList = [];
        for(var key in users){
            if(users[key].status === 'student'){
                users[key].name = users[key].firstName + " " + users[key].lastName
                studentList.push(users[key])
            }
            this.setState({
                studentList,
                copyStudentList: studentList
            })
        }
    }

    deleteUser = (index) =>{
        let { studentList } = this.state;
        fetch(`https://rocky-stream-13314.herokuapp.com/deleteUser/${studentList[index]._id}`,{
            method : 'DELETE'
        })
        ToastAndroid.showWithGravityAndOffset(`${studentList[index].firstName} Remove From Student List`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        studentList.splice(index,1)
        this.setState({studentList})
    }

    searchFilter(text){
        let search = text.toLowerCase();
        this.setState({
            copyStudentList: this.state.studentList.filter(obj => obj.name.toLowerCase().includes(search))
        });
    }

    renderStudentList({ item, index }){
        return(
            <View style={{marginHorizontal: 5, marginVertical: 8,}}>
                <View style={{marginHorizontal: 2, borderRadius:10, backgroundColor: '#fff',padding: 15,shadowColor: '#000', shadowOpacity: 0.55, shadowRadius: 2.98, elevation: 2, flexDirection: 'column'}}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{width: '93%', fontSize: 15, fontFamily: 'Raleway-Light'}}><Text style={{fontFamily: 'Raleway-Regular'}}>NAME</Text>: { item.firstName.toUpperCase() +" "+ item.lastName.toUpperCase() }</Text>
                        <TouchableHighlight onPress={()=>this.deleteUser(index)}>
                            <IconCross size={23} name='cross' style={{alignItems: 'center'}} color='red'/>
                        </TouchableHighlight>                          
                    </View>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>Email<Text style={{fontFamily: 'Raleway-Light'}}>: { item.email }</Text></Text>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>DESTINATION<Text style={{fontFamily: 'Raleway-Light'}}>: UNIVERSITY OF KARACHI</Text></Text>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>STATUS<Text style={{fontFamily: 'Raleway-Light'}}>: { item.status.toUpperCase() }</Text></Text>                        
                </View>  
            </View>
        )
    }
    render() {
        let { studentList, copyStudentList } = this.state;
    //    var updateList = studentList.concat([])
      return (
        <Container>
            <View style={styles.adminsearch}>
                <Item style={styles.textField}>
                        <Input placeholder = 'Search By Name' onChangeText={(val)=>this.searchFilter(val)} style={styles.underline} placeholderTextColor="#C0C0C0"/>
                        <Icon name='search' size={20} color='green' />
                </Item>
            </View>
            {
                copyStudentList.length !== 0 ? 
                <ScrollView>
                    <FlatList
                        data={copyStudentList}
                        renderItem={this.renderStudentList.bind(this)}
                        keyExtractor={( item, index ) => item.email}/>
                </ScrollView>:
               <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}><Text style={{ fontSize: 17, fontFamily: 'Raleway-Light' }}> No Result Found </Text></View> 
            }
        </Container>
      );
    }
  }
  
  function mapStateToProps(state){
    return{
        allUsers: state.AUTHENTICATION.allUser   
    }
}

function mapDispatchToProps(dispatch){
  return{}
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentList)
