import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, AsyncStorage, ImageBackground } from "react-native";
import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class DrawerMenu extends Component{
  constructor(){
    super();
    this.state= {
        name: '',
        email: '',
        status: '',
      categoryNav: []
    }
  }
  componentDidMount(){
      this.getData();
  }
  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('user').then(val => {
        if (val !== null) {
          let parsed = JSON.parse(val);
          this.setState({
              name: parsed.firstName+" "+ parsed.lastName,
              email: parsed.email,
              status: parsed.status
          })
          let categoryNav = [
            {title: 'HOME', link: 'Home', icon: 'home'},
            this.state.status === 'driver' ? {title: 'POINT LOCATION', link: 'DriverLocation', icon: 'location-pin'}: {title: 'TRACK POINT', link: 'TraceLocation', icon: 'location-pin'},
            {title: 'SETTINGS', link: 'SettingScreen', icon: 'settings'},
            {title: 'LOGOUT',icon: 'log-out', func: () => {
                AsyncStorage.removeItem('user')
                this.props.navigation.navigate('Login')
            }
            }
          ]
          this.setState({categoryNav})
        }
      })
    }
    catch (e) {}
  }

  onSelectItem(item) {
    if(item.link) {
        this.props.navigation.navigate(item.link) 
    }
    else item.func();
}

  renderMenuItem({ item }) {
    return (
        <TouchableOpacity style={{ flex: 1,paddingHorizontal: 15,}} onPress={() => this.onSelectItem(item)}>
            <View style={{ paddingVertical:  12,flexDirection: 'row',alignItems: 'center'}}>
                {item.icon === 'location-pin' || item.icon === 'log-out' ? <Icon  name={item.icon} size={25} color="green"/> : <MaterialIcons  name={item.icon} size={25} color="green"/>}
                <Text style={{ fontSize: 15.3,color: '#2a2a2a', flex: 1, marginHorizontal: 15}}>{item.title}</Text>
            </View>
        </TouchableOpacity>
    );
}
  render(){
      const {name, email, status} = this.state
    return(
      <View style = {{backgroundColor: '#f2f2f2', flex:1}}>
          <ImageBackground style={{width: '100%', height: 175, justifyContent: 'center'}} source={require('../Images/drawer-banner.png')}>
              <View style={{flexDirection: 'row', paddingLeft: 20,paddingRight: 10}}>
                  <Image style={{ width: 70, height: 70, marginRight: 15}} source={require('../Images/avatar.png')} />
                  <View style={{flex: 1}}>
                      <Text style={{color: 'green', fontSize: 18, fontWeight: 'bold', marginTop: 12, marginBottom: 3}}>{name}</Text>
                      <Text style={{color: 'green', fontSize: 14, width: '100%'}}>{email}</Text>
                      <Text style={{color: 'green', fontSize: 14, width: '100%'}}>{status}</Text>                      
                  </View>
              </View>
          </ImageBackground>
      <ScrollView>
          <View style={{flex: 1, paddingHorizontal: 15}}>
                <FlatList
                  data={this.state.categoryNav}
                  renderItem={this.renderMenuItem.bind(this)}
                  keyExtractor={(item)=> item.title}
                  />
          </View>
      </ScrollView>
  </View>
    )
  }
  
}