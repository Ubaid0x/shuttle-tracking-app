import React from 'react';
import {Platform,StyleSheet,Text, Alert, View} from 'react-native';
import {Container , Content , Header , Body , Icon ,Left} from 'native-base';
import {connect} from 'react-redux'
import io from 'socket.io-client/dist/socket.io';
import MapView from 'react-native-maps';
import TraceStyle from './TraceLocationStyle';

class TraceLocation extends React.Component{
    constructor(){
        super();
        this.state ={
            mapRegion: null,
            lastLong: null,
            lastLat: null,
            position: [],
        }      
        this.socket = io.connect('https://rocky-stream-13314.herokuapp.com', {jsonp: false});
    }
    render(){
        let { driverId } = this.props.navigation.state.params;
        var self = this;
        this.socket.on('location1', function(data){  
            if(driverId === data.id){
                let region = {
                    latitude : data.position.coords.latitude,
                    longitude : data.position.coords.longitude,
                    latitudeDelta : 0.00922*1.5,
                    longitudeDelta : 0.00421*1.5
                }
                   self.setState({
                       mapRegion: region,
                       lastLat: region.latitude,
                       lastLong: region.longitude
                })    
            }
        })
        return(
                <MapView style={TraceStyle.map}
                        region={this.state.mapRegion}
                        showsUserLocation={true}
                        followsUserLocation={true}>
                    <MapView.Marker draggable
                        coordinate={{
                        latitude: (this.state.lastLat) || -36.82339,
                        longitude: (this.state.lastLong) || -73.03569,
                        }}>
                    </MapView.Marker>
                </MapView> 
        )
    }
}

function mapStateToProps(state){
    return{
        location:  state.UPDATELOCATION
    }
}
function mapDispatchToProps(){
    return{
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TraceLocation)